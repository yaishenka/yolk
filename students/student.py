from string import punctuation

class StudentException(Exception):
    pass


class Student:
    required_fields = ['name', 'group', 'gitlab_login']

    def __init__(self, default_header: dict):
        for required_field in self.required_fields:
            if required_field not in default_header.keys():
                raise StudentException(f'Field {required_field} must be in default_header')

        for key, value in default_header.items():
            self.__dict__[key] = value

        self.__dict__['pretty_gitlab_login'] = self.prettify_gitlab_login(self.gitlab_login)

    @staticmethod
    def prettify_gitlab_login(gitlab_login):
        specials = set(punctuation)
        pretty = False
        while not pretty:
            founded = False
            for c in specials:
                if gitlab_login.startswith(c):
                    founded = True
                    gitlab_login = gitlab_login.replace(c, '', 1)

            pretty = not founded

        return gitlab_login

    @property
    def name(self):
        return self.__dict__['name']

    @property
    def group(self):
        return self.__dict__['group']

    @property
    def gitlab_login(self):
        return self.__dict__['gitlab_login']

    @property
    def reviewers(self):
        reviewers = self.__dict__.get('reviewer', None)
        if reviewers is None:
            return []

        if not reviewers:
            return []

        return reviewers.split(',')

    def set(self, values):
        for field, value in values.items():
            if field not in self.__dict__:
                raise LookupError(f"Field {field} not in Student")
            self.__dict__[field] = value

    def __repr__(self):
        return str(self.__dict__)
