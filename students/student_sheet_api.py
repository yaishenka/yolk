import logging
from collections.abc import Iterable

from students.student import Student

from google_sheets_integration.google_sheets_reader import (GoogleSheetsError,
                                                            GoogleSheetsReader)


class StudentSheetApiError(Exception):
    pass


class StudentSheetApi:
    def __init__(
        self,
        table_url: str,
        creds_file_path: str,
        sheet_to_work_in: int = 0,
        logger=None,
    ):
        self.logger = logger if logger else logging.getLogger(__name__)
        self.table_url = table_url
        try:
            self.sheets_reader = GoogleSheetsReader(creds_file_path, logger)
        except GoogleSheetsError as e:
            self.logger.error(f'Failed to build google sheets reader. Error: {e}')
            raise StudentSheetApiError('Failed to build google sheets reader') from e
        self.sheet_to_work_in = sheet_to_work_in
        self.table_header = self._get_table_header()
        self.key_to_index = self._get_key_to_index()

    def get_student(
        self,
        values_to_find: dict,
        row_to_start: int = 1,
    ) -> [Student, None]:
        student, _ = self._get_student(values_to_find, row_to_start)
        return student

    def get_all_students(
        self,
        row_to_start: int = 1,
    ) -> list[Student]:
        try:
            all_values = self.sheets_reader.get_all_values(self.table_url, self.sheet_to_work_in)
        except Exception as e:
            error_msg = f'Failed to get all values from table {self.table_url} sheet {self.sheet_to_work_in}: {e}'
            self.logger.error(error_msg)
            raise StudentSheetApiError(error_msg) from e

        students = []
        for row in all_values[row_to_start:]:
            is_empty = True
            for value in row:
                if value:
                    is_empty = False
            if is_empty:
                break
            students.append(Student(dict(zip(self.table_header, row))))

        return students

    def update_student(
        self,
        values_to_find: dict,
        values_to_update: dict,
        row_to_start: int = 1,
    ) -> bool:
        self._check_keys(values_to_update.keys())

        student, position = self._get_student(values_to_find, row_to_start)
        if not student:
            return False

        worksheet = self.sheets_reader.get_worksheet(self.table_url, self.sheet_to_work_in)
        for key, value in values_to_update.items():
            try:
                worksheet.update_cell(position + 1, self.key_to_index[key] + 1, value)
            except Exception as e:
                self.logger.error(f'Failed to update student {student} key {key}: {e}')
                return False

        return True

    def update_students_batch(
        self,
        students_to_update_dict: dict[Student, dict],
        row_to_start: int = 1,
    ) -> dict[Student, bool]:
        result = {student: False for student in students_to_update_dict}

        worksheet = self.sheets_reader.get_worksheet(self.table_url, self.sheet_to_work_in)
        cells_list = self.sheets_reader.get_all_cells(
            self.table_url,
            self.sheet_to_work_in,
            row_to_begin=row_to_start,
        )
        columns_count = len(self.table_header)
        for row in range(0, len(cells_list), columns_count):
            for student in students_to_update_dict:
                matched = True
                for key in student.required_fields:
                    if cells_list[row + self.key_to_index[key]].value != student.__dict__[key]:
                        matched = False
                        break
                if not matched:
                    continue

                for key, value in students_to_update_dict[student].items():
                    cells_list[row + self.key_to_index[key]].value = value

                result[student] = True
        worksheet.update_cells(cells_list)

        return result

    def _get_table_header(self):
        try:
            table_header = self.sheets_reader.get_header(self.table_url, self.sheet_to_work_in)
        except Exception as e:
            error_msg = f'Failed to get header from table {self.table_url} sheet {self.sheet_to_work_in}: {e}'
            self.logger.error(error_msg)
            raise StudentSheetApiError(error_msg) from e

        return table_header

    def _get_key_to_index(self):
        key_to_index = {}
        for i, key in enumerate(self.table_header):
            key_to_index[key] = i

        return key_to_index

    def _get_student(
        self,
        values_to_find: dict,
        row_to_start: int = 1,
    ):
        self._check_keys(values_to_find.keys())

        try:
            all_values = self.sheets_reader.get_all_values(self.table_url, self.sheet_to_work_in)
        except Exception as e:
            error_msg = f'Failed to get all values from table {self.table_url} sheet {self.sheet_to_work_in}: {e}'
            self.logger.error(error_msg)
            raise StudentSheetApiError(error_msg) from e

        founded = False
        founded_index = None
        for i, row in enumerate(all_values[row_to_start:]):
            matched = True
            for key_to_find, value_to_find in values_to_find.items():
                if row[self.key_to_index[key_to_find]] != value_to_find:
                    matched = False
                    break
            if not matched:
                continue

            if founded:
                error_msg = f'Duplicate founded. Rows {founded_index} and {i} matched'
                self.logger.error(error_msg)
                raise StudentSheetApiError(error_msg)

            founded = True
            founded_index = i

        if not founded:
            self.logger.error(f'Student with fields {values_to_find} no exists')
            return None, None

        s = Student(dict(zip(self.table_header, all_values[row_to_start + founded_index])))
        return s, row_to_start + founded_index

    def _check_keys(self, keys: Iterable):
        for key in keys:
            if key not in self.table_header:
                self.logger.error(f'Key {key} not in table_header')
                raise StudentSheetApiError(f'Key {key} not in table_header')
