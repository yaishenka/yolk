import os


class ConfigException(Exception):
    pass


class ConfigHelper:
    CONFIG_FOLDER_ENV = 'YOLK_CONFIG_FOLDER'
    COURSE_FOLDER = 'courses'
    REPOS_FOLDER = 'repos'
    PLAGIARISM_FOLDER = 'plagiarism'

    def __init__(self, path_to_main_configs_folder: str):
        if not os.path.exists(path_to_main_configs_folder):
            raise ConfigException(f'Folder {path_to_main_configs_folder} not exist')

        self.path_to_main_configs_folder = path_to_main_configs_folder

    def get_course_config_path(self, config_name: str):
        if not config_name.endswith('.json'):
            config_name += '.json'

        return os.path.join(
            self.path_to_main_configs_folder,
            self.COURSE_FOLDER,
            config_name
        )

    def get_repo_config_path(self, config_name: str):
        if not config_name.endswith('.json'):
            config_name += '.json'

        return os.path.join(
            self.path_to_main_configs_folder,
            self.REPOS_FOLDER,
            config_name
        )

    def get_plagiarism_config_path(self, config_name: str):
        if not config_name.endswith('.json'):
            config_name += '.json'

        return os.path.join(
            self.path_to_main_configs_folder,
            self.PLAGIARISM_FOLDER,
            config_name
        )

    def get_google_auth_file(self, file_name: str):
        if not file_name.endswith('.json'):
            file_name += '.json'

        return os.path.join(
            self.path_to_main_configs_folder,
            file_name
        )

    @staticmethod
    def build():
        path_to_main_configs_folder = os.getenv(ConfigHelper.CONFIG_FOLDER_ENV, None)

        if path_to_main_configs_folder is None:
            raise ConfigException(f'You should set {ConfigHelper.CONFIG_FOLDER_ENV}')

        return ConfigHelper(path_to_main_configs_folder)
