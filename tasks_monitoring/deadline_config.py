import os
import json
import logging
from datetime import datetime


class DeadlineConfigException(Exception):
    pass


class DeadlineConfig:
    def __init__(
        self,
        daily_fine_percent: float,
        max_fine: float,
        timestamp_format: str,  # "%d.%m.%Y %H:%M:%S" for example
        task_to_deadline_str: dict[str, str],
        hard_deadline: [None,str],
        logger=None,
    ):
        self.daily_fine_percent = daily_fine_percent
        self.max_fine = max_fine
        self.timestamp_format = timestamp_format
        self.hard_deadline = None if hard_deadline is None else datetime.strptime(
            hard_deadline,
            self.timestamp_format,
        )
        self.logger = logger if logger else logging.getLogger(__name__)

        self.task_to_deadline = {}
        for task, datetime_in_str in task_to_deadline_str.items():
            self.task_to_deadline[task] = datetime.strptime(
                datetime_in_str,
                self.timestamp_format,
            )

    def get_fine_for_task(self, task_name, date):
        if task_name not in self.task_to_deadline:
            return 0

        if date <= self.task_to_deadline[task_name]:
            return 0

        delta = date - self.task_to_deadline[task_name]
        return min(self.max_fine, (self.daily_fine_percent / 24) * (delta.total_seconds() // 3600))

    def is_after_hard_deadline(self, task_name, date):
        if self.hard_deadline is None:
            return False
        
        return date > self.hard_deadline

    @staticmethod
    def build_from_json(path_to_json: str, logger=None):
        if not os.path.exists(path_to_json):
            raise DeadlineConfigException(f'No file {path_to_json}')

        with open(path_to_json, 'r') as json_file:
            json_dict = json.load(json_file)

        return DeadlineConfig.build_from_dict(json_dict, logger)

    @staticmethod
    def build_from_dict(json_dict: dict, logger=None):
        return DeadlineConfig(
            json_dict['daily_fine_percent'],
            json_dict['max_fine'],
            json_dict['timestamp_format'],
            json_dict['task_to_deadline_str'],
            json_dict.get('hard_deadline', None),
            logger,
        )

    @staticmethod
    def get_empty_deadline_config(logger=None):
        return DeadlineConfig(
            0,
            0,
            "",
            {},
            logger
        )
