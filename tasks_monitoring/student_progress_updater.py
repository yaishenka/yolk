import logging
import json
from datetime import datetime, timedelta
from students.student import Student
from course_manager import course_config
from gitlab_integration.gitlab_api import GitlabApi
from tasks_monitoring.deadline_config import DeadlineConfig


class ProgressUpdaterException(Exception):
    pass


class StudentProgressUpdater:
    def __init__(
        self,
        course_config_name: str,
        logger=None,
    ):
        self.logger = logger if logger else logging.getLogger(__name__)
        self.course_config = course_config.CourseConfig.build_from_env(
            course_config_name,
            self.logger
        )
        self.repo_config = self.course_config.get_repo_config()

        self._students_sheets_api = self.course_config.get_student_sheet_api()
        self._gitlab_api = GitlabApi(self.repo_config.gitlab_token, self.repo_config.gitlab_url)

        self.deadline_config = self._get_deadlines_config()

    def update(self):
        all_students = self._students_sheets_api.get_all_students()
        to_update = {}

        for student in all_students:
            self.logger.info(f'Update progress for student {student}')
            tasks_to_update = self._get_tasks_to_update(student)
            to_update[student] = tasks_to_update

        self._students_sheets_api.update_students_batch(to_update)

    def _get_tasks_to_update(self, student: Student):
        result = {}
        student_project_name = self.course_config.get_student_repo_name(
            student,
        )
        all_mrs = self._gitlab_api.get_all_merge_requests(
            self.repo_config.namespace_to_create_repos,
            student_project_name,
        )
        for mr in all_mrs:
            first_success_pipeline = self._get_first_success_pipeline(mr)
            if first_success_pipeline is None:
                self.logger.debug(f'No success pipelines for mr {mr}')
                continue
            pipeline_date = first_success_pipeline.created_at
            pipeline_date = datetime.fromisoformat(pipeline_date.replace('Z', ''))
            pipeline_date += timedelta(hours=3)

            task_name = mr.source_branch
            final_score_percent = round(1 - self.deadline_config.get_fine_for_task(task_name, pipeline_date), 2)
            if self.deadline_config.is_after_hard_deadline(task_name, pipeline_date):
                self.logger.info(f"Get task after deadline, user {student}")
                final_score_percent = 0

            prev_result = result.get(task_name)
            prev_score = 0.0
            merged_in_prev = prev_result is not None and '?' not in str(prev_result)
            if prev_result is not None:
                prev_score = float(str(prev_result).replace('?', ''))

            final_score_percent = max(final_score_percent, prev_score)
            merged = mr.state == 'merged' or merged_in_prev

            if not merged:
                result[task_name] = f'?{final_score_percent}?'
            else:
                result[task_name] = final_score_percent

        return result

    @staticmethod
    def _get_first_success_pipeline(mr):
        pipelines = mr.pipelines.list(get_all=True)
        pipelines.sort(key=lambda x: datetime.fromisoformat(x.created_at.replace('Z', '')))
        for pipeline in pipelines:
            if pipeline.status == 'success':
                return pipeline

        return None

    def _get_deadlines_config(self):
        gitlab_file = self._gitlab_api.get_file_if_exist(
            self.course_config.course_repo_namespace,
            self.course_config.course_repo_name,
            self.course_config.course_main_branch,
            self.course_config.deadlines_file_path,
        )
        if not gitlab_file:
            self.logger.error('Fail to get deadlines config file')
            return DeadlineConfig.get_empty_deadline_config(self.logger)

        json_dict = json.loads(gitlab_file.content)
        return DeadlineConfig.build_from_dict(json_dict, self.logger)
