import argparse
from scripts.create_repos import prepare_repo_manager

SCRIPT_NAME = "update_access"


def main():
    argument_parser = argparse.ArgumentParser(description=('Update access for all students'))
    _, repo_manager = prepare_repo_manager(argument_parser, SCRIPT_NAME)
    repo_manager.update_access_for_all_repos()


if __name__ == '__main__':
    main()
