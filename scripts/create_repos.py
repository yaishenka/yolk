import logging
import argparse
from course_manager import students_repo_manager
from scripts import helpers

SCRIPT_NAME = "create_repos"


def prepare_repo_manager(argument_parser, script_name=SCRIPT_NAME):
    helpers.add_default_arguments(argument_parser)
    argument_parser.add_argument(
        '--set-creation-status',
        action='store_true',
        dest='set_creation_status',
        default=False,
        help='set creation_status field in google table'
    )
    argument_parser.add_argument(
        '--set-repo_url',
        action='store_true',
        dest='set_repo_url',
        default=False,
        help='set repo_url field in google table'
    )
    args = argument_parser.parse_args()
    logger = logging.getLogger(script_name)
    helpers.common_main(args, logger, script_name)

    repo_manager = students_repo_manager.StudentsRepoManager(
        args.course_config,
        logger,
        set_creation_status=args.set_creation_status,
        set_repo_url=args.set_repo_url,
    )

    return args, repo_manager


def main():
    argument_parser = argparse.ArgumentParser(description=('Create repos for all students'))

    _, repo_manager = prepare_repo_manager(argument_parser)
    repo_manager.create_all_repos()


if __name__ == '__main__':
    main()
