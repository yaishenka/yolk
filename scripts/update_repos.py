import argparse
from scripts.create_repos import prepare_repo_manager

SCRIPT_NAME = "update_repos"


def main():
    argument_parser = argparse.ArgumentParser(description=('Update repos for all students'))
    argument_parser.add_argument(
        '--force',
        action='store_true',
        default=False,
    )
    args, repo_manager = prepare_repo_manager(argument_parser, SCRIPT_NAME)
    repo_manager.update_all_repos(force=args.force)


if __name__ == '__main__':
    main()
