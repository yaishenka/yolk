import logging
import argparse
from moss_integration import plagiarism_checker
from scripts import helpers

SCRIPT_NAME = "create_plagiarism_report"


def main():
    argument_parser = argparse.ArgumentParser(description='Create plagiarism report')
    helpers.add_default_arguments(argument_parser)
    argument_parser.add_argument(
        '--no-downloading',
        action='store_true',
        dest='no_downloading',
        default=False,
        help='Do not download tasks from gitlab'
    )
    argument_parser.add_argument(
        '--no-sending-to-moss',
        action='store_true',
        dest='no_moss',
        default=False,
        help='Do not create reports from moss'
    )
    args = argument_parser.parse_args()
    logger = logging.getLogger(SCRIPT_NAME)
    helpers.common_main(args, logger, SCRIPT_NAME)

    checker = plagiarism_checker.PlagiarismChecker(
        args.course_config,
        logger
    )

    if not args.no_downloading:
        checker.download_data()
    if not args.no_moss:
        checker.create_report()
    checker.create_google_sheets_report()


if __name__ == '__main__':
    main()
