import logging
import argparse
from tasks_monitoring import student_progress_updater
from scripts import helpers

SCRIPT_NAME = "update_students_progress"


def main():
    argument_parser = argparse.ArgumentParser(description=('Update students progress'))
    helpers.add_default_arguments(argument_parser)
    args = argument_parser.parse_args()
    logger = logging.getLogger(SCRIPT_NAME)
    helpers.common_main(args, logger, SCRIPT_NAME)

    updater = student_progress_updater.StudentProgressUpdater(
        args.course_config,
        logger
    )
    updater.update()


if __name__ == '__main__':
    main()
