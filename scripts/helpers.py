import logging
import os
from datetime import datetime
from configs_helper.config_helper import ConfigHelper


def add_default_arguments(argument_parser):
    argument_parser.add_argument(
        '--config-folder',
        dest='config_folder',
        required=True,
        type=str,
        help='path to main config folder')
    argument_parser.add_argument(
        '--verbose',
        action='store_true',
        default=False,
    )
    argument_parser.add_argument(
        '--course-config',
        dest='course_config',
        required=True,
        type=str,
        help='course config name'
    )


def common_main(args, logger, script_name):
    os.environ[ConfigHelper.CONFIG_FOLDER_ENV] = args.config_folder
    set_logging(
        'logs',
        get_log_name(script_name),
        args.verbose,
        logger
    )


def set_logging(log_path: str, file_name: str, verbose=False, logger=None):
    if not os.path.exists(log_path):
        os.mkdir(log_path)

    logger = logger if logger is not None else logging.getLogger()
    level = logging.DEBUG if verbose else logging.ERROR
    logger.setLevel(level)

    log_formatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
    file_handler = logging.FileHandler(f"{log_path}/{file_name}.log", mode='w')
    file_handler.setFormatter(log_formatter)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    logger.addHandler(console_handler)


def get_log_name(basic_name: str):
    return basic_name + '_' + datetime.now().strftime('%m.%d.%Y_%H:%M.log')
