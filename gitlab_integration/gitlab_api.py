from __future__ import annotations

import logging
import os
import tempfile
from collections import namedtuple
from enum import Enum

from git import Repo, Submodule
import gitlab

DEFAULT_GITLAB_URL = 'https://gitlab.com'

GitlabFile = namedtuple("GitlabFile", [
    'path',
    'content',
])


class GitlabFileStatus(Enum):
    CREATED = 0
    UPDATED = 1
    NOT_UPDATED = 2
    FAILED = 3


class GitlabApiError(Exception):
    pass


class GitlabApi:
    def __init__(self, token: str, gitlab_url=DEFAULT_GITLAB_URL, logger=None):
        self.gitlab_url = gitlab_url
        self.logger = logger if logger else logging.getLogger(__name__)
        self._token = token
        self._gitlab_api = gitlab.Gitlab(gitlab_url, token)

    def is_project_exists(self, namespace: str, project_name: str) -> bool:
        project = self._get_project(namespace, project_name)
        if project is None:
            return False

        return True

    def get_project_url(self, namespace: str, project_name: str) -> [str, None]:
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        return project.http_url_to_repo

    def get_project_branches(self, namespace: str, project_name: str):
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        return project.branches.list(get_all=True)

    def get_project_tree(self, namespace: str, project_name: str, branch='master'):
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        return project.repository_tree(ref=branch, all=True, recursive=True)

    def get_branch_files(
        self,
        namespace: str,
        project_name: str,
        branch: str,
        files_to_ignore=None,
        in_bytes=False,
    ) -> [list[GitlabFile], None]:
        if files_to_ignore is None:
            files_to_ignore = set()
        try:
            project_tree = self.get_project_tree(namespace, project_name, branch)
        except Exception as e:
            self.logger.error(
                f'Failed to get project tree for project {project_name} in namespace {namespace}. Exception: {e}')
            return None
        if project_tree is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        files = []
        for file in project_tree:
            if file['type'] != 'blob':
                continue
            if file['name'] in files_to_ignore:
                continue
            files.append(self.get_file_if_exist(
                namespace,
                project_name,
                branch,
                file['path'],
                in_bytes,
            ))

        return files

    def create_project(
        self,
        namespace: str,
        project_name: str,
        default_branch='master',
    ) -> [str, None]:
        project = self._get_project(namespace, project_name)
        if project is not None:
            self.logger.info(f'Project {project_name} already in namespace {namespace}')
            return project.http_url_to_repo.replace('.git', '')

        try:
            project = self._gitlab_api.projects.create({
                'name': project_name,
                'default_branch': default_branch
            })
            return project.http_url_to_repo.replace('.git', '')
        except gitlab.exceptions.GitlabError as e:
            self.logger.error(f'Error while creating repository {project_name}. Error: {e}')
            return None

    def add_submodule_to_project(
        self,
        namespace: str,
        project_name: str,
        submodule_url: str,
        submodule_name: str,
        submodule_path: str,
        default_branch='main',
    ) -> [str, None]:
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        url_to_clone = self._get_url_to_clone_with_token(project)

        with tempfile.TemporaryDirectory() as directory:
            repo_path = os.path.join(directory, project_name)
            try:
                git_repo = Repo.clone_from(url_to_clone, repo_path, branch=default_branch)
            except Exception as e:
                self.logger.error(f'Cloning repo {project_name} failed. Error: {e}')
                return None

            try:
                Submodule.add(git_repo, submodule_name, submodule_path, submodule_url)
            except Exception as e:
                self.logger.error(f'Adding submodule {submodule_url} to repo {project_name} failed.'
                                  f'Error: {e}')
                return None

            try:
                git_repo.git.add(update=True)
                git_repo.index.commit(f'Add submodule {submodule_name}')
                origin = git_repo.remote(name='origin')
                origin.push()
            except Exception as e:
                self.logger.error(f'Commiting to repo {project_name} failed. Error: {e}')
                return None

        return submodule_url

    def remove_access(
        self,
        namespace: str,
        project_name: str,
        gitlab_login: str,
    ) -> [str, None]:
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        gitlab_user = self._get_gitlab_user(gitlab_login)
        if not gitlab_user:
            self.logger.error(f'No gitlab user with login {gitlab_login}')
            return None

        try:
            project_member = project.members.get(gitlab_user.id)
            project_member.delete()
            return gitlab_login
        except gitlab.exceptions.GitlabGetError:
            self.logger.info(f'No member {gitlab_login} in project')
            return gitlab_login
        except gitlab.GitlabError as e:
            self.logger.error(f'Fail to remove {gitlab_login}. Error {e}')
            return None

    def give_access(
        self,
        namespace: str,
        project_name: str,
        gitlab_login: str,
        access_level: int  # gitlab.const.AccessLevel
    ) -> [gitlab.const.AccessLevel, None]:
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        gitlab_user = self._get_gitlab_user(gitlab_login)
        if not gitlab_user:
            self.logger.error(f'No gitlab user with login {gitlab_login}')
            return None

        try:
            project_member = project.members.get(gitlab_user.id)
            if project_member.access_level > access_level:
                return project_member.access_level

            project_member.access_level = access_level
            project_member.save()
            return access_level
        except gitlab.exceptions.GitlabGetError:
            pass

        try:
            project.members.create({
                "user_id": gitlab_user.id,
                "project_id": project.id,
                "access_level": access_level,
            })
        except gitlab.GitlabError as e:
            self.logger.error(f'Fail to add {gitlab_login} to {project_name}. Error {e}')
            return None

        return access_level

    def add_runner_to_project(
        self,
        namespace: str,
        project_name: str,
        runner_id: str
    ) -> [str, None]:
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        try:
            runner = self._gitlab_api.runners.get(runner_id)
        except gitlab.exceptions.GitlabGetError:
            self.logger.error(f'No runner with id {runner_id}')
            return None

        try:
            p_runner = project.runners.create({'runner_id': runner.id})
        except gitlab.exceptions.GitlabCreateError as e:
            if b'has already been taken' in e.response_body:
                self.logger.info(f'Runner {runner_id} already in project {project_name}')
                return runner_id
            self.logger.error(f'Register of runner {runner_id} failed. Error {e}')
            return None

        return p_runner.id

    def disable_shared_runners(
        self,
        namespace: str,
        project_name: str,
    ) -> [bool, None]:
        return self._set_shared_runners_setting(namespace, project_name, False)

    def enable_shared_runners(
        self,
        namespace: str,
        project_name: str,
    ) -> [bool, None]:
        return self._set_shared_runners_setting(namespace, project_name, True)

    def set_path_to_ci_file(
        self,
        namespace: str,
        project_name: str,
        path_to_ci_file: str,
    ) -> [str, None]:
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        try:
            project.ci_config_path = path_to_ci_file
            project.save()
            return path_to_ci_file
        except gitlab.exceptions.GitlabError as e:
            self.logger.error(f'Updating ci_config_path failed. Error {e}')
            return None

    def create_branch(
        self,
        namespace: str,
        project_name: str,
        branch_name: str,
        ref_branch: str,
    ) -> [str, None]:
        if branch_name in {'main', 'master'}:
            raise GitlabApiError('You should not create default branch manually')

        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        try:
            project.branches.get(branch_name)
            self.logger.info(f'Branch {branch_name} exists')
            return branch_name
        except gitlab.exceptions.GitlabGetError:
            pass

        try:
            project.branches.create({
                'branch': branch_name,
                'ref': ref_branch,
            })
            return branch_name
        except gitlab.exceptions.GitlabCreateError as e:
            self.logger.error(f'Creating branch {branch_name} failed. Error {e}')
            return None

    def add_files_to_branch(
        self,
        namespace: str,
        project_name: str,
        branch_name: str,
        files: list[GitlabFile],
        update_files=False,
        default_branch_name='main',
    ) -> [list[tuple[GitlabFile, GitlabFileStatus]], None]:
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        if branch_name != default_branch_name:
            try:
                project.branches.get(branch_name)
            except gitlab.exceptions.GitlabGetError:
                self.logger.error(f'You must create branch {branch_name} first')
                return None

        result = []
        for file in files:
            gitlab_file = self._get_file_if_exists(project, branch_name, file.path)
            if gitlab_file is not None:
                if not update_files:
                    result.append((file, GitlabFileStatus.NOT_UPDATED))
                    continue
                result.append((file, self._update_file(file, gitlab_file, branch_name)))
                continue

            try:
                project.files.create({
                    'file_path': file.path,
                    'branch': branch_name,
                    'content': file.content,
                    'commit_message': f'Create file {file.path}',
                })
                result.append((file, GitlabFileStatus.CREATED))
            except gitlab.exceptions.GitlabCreateError as e:
                self.logger.error(f'Creating file {file.path} in branch {branch_name} failed. '
                                  f'Error {e}')
                result.append((file, GitlabFileStatus.FAILED))

        return result

    def set_ci_var(
        self,
        namespace: str,
        project_name: str,
        variable_key: str,
        variable_value: str,
        force_update: bool = True,
        protected: bool = False,  # use only in protected branches
        masked: bool = True,  # mask in logs
    ):
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None
        try:
            p_var = project.variables.get(variable_key)
            self.logger.info(f'Variable {variable_key} already in {project_name}')
            if force_update:
                self.logger.info(f'Updating variable {variable_key}')
                p_var.value = variable_value
                p_var.protected = protected
                p_var.masked = masked
                p_var.save()
            return variable_key
        except gitlab.exceptions.GitlabGetError:
            pass

        try:
            project.variables.create({
                'key': variable_key,
                'value': variable_value,
                'protected': protected,
                'masked': masked
            })
        except Exception as e:
            self.logger.error(f'Fail to create variable {variable_key} in project {project_name}: {e}')
            return None

        return variable_key

    def get_all_merge_requests(
        self,
        namespace: str,
        project_name: str,
    ):
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        return project.mergerequests.list()

    def _update_file(self, file: GitlabFile, gitlab_file, branch_name: str) -> GitlabFileStatus:
        try:
            gitlab_file.content = file.content
            gitlab_file.save(branch=branch_name,
                             commit_message=f'Updating file {file.path}')
            return GitlabFileStatus.UPDATED
        except gitlab.exceptions.GitlabError as e:
            self.logger.error(f'Updating file {file.path} in branch {branch_name} failed. '
                              f'Error {e}')
            return GitlabFileStatus.FAILED

    def get_file_if_exist(
        self,
        namespace: str,
        project_name: str,
        branch_name: str,
        path: str,
        in_bytes=False,
    ) -> [GitlabFile, None]:
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        gitlab_file = self._get_file_if_exists(project, branch_name, path)
        if gitlab_file is None:
            return None

        if in_bytes:
            return GitlabFile(path, gitlab_file.decode())

        return GitlabFile(path, gitlab_file.decode().decode('utf-8'))

    def get_default_branch_name(
        self,
        namespace: str,
        project_name: str,
    ) -> [str, None]:
        branches = self.get_project_branches(namespace, project_name)
        if not branches:
            return None

        for branch in branches:
            if branch.default:
                return branch.name

        return None

    @staticmethod
    def _get_file_if_exists(
        project,
        branch_name: str,
        path: str,
    ):
        try:
            return project.files.get(file_path=path, ref=branch_name)
        except gitlab.exceptions.GitlabGetError:
            return None

    def _set_shared_runners_setting(
        self,
        namespace: str,
        project_name: str,
        status: bool,
    ) -> [bool, None]:
        project = self._get_project(namespace, project_name)
        if project is None:
            self.logger.error(f'Project {project_name} in namespace {namespace} not exist')
            return None

        try:
            project.shared_runners_enabled = status
            project.save()
            return status
        except gitlab.exceptions.GitlabError as e:
            self.logger.error(f'Updating shared_runners_setting failed. Error {e}')
            return None

    @staticmethod
    def get_project_name_by_url(project_url: str):
        return project_url.split('/')[-1]

    def _get_gitlab_user(self, gitlab_login: str):
        try:
            return self._gitlab_api.users.list(username=gitlab_login)[0]
        except Exception:
            return None

    def _get_url_to_clone_with_token(self, project):
        project_url = project.http_url_to_repo.split(self.gitlab_url)[1]
        gitlab_api_splitted = self.gitlab_url.split('https://')[1]
        return f'https://oauth2:{self._token}@{gitlab_api_splitted}/{project_url}'

    def _get_project(self, namespace: str, project_name: str):
        if namespace:
            project_name_with_namespace = f'{namespace}/{project_name}'
        else:
            project_name_with_namespace = project_name
        try:
            return self._gitlab_api.projects.get(project_name_with_namespace)
        except gitlab.exceptions.GitlabGetError:
            return None
