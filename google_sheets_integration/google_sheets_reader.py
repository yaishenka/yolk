import logging
import os
import string

import gspread
from oauth2client.service_account import ServiceAccountCredentials


class GoogleSheetsError(Exception):
    pass


class GoogleSheetsReader:
    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']
    alphabet = string.ascii_uppercase

    def __init__(self, credentials_file_path: str, logger=None):
        self.logger = logger if logger else logging.getLogger(__name__)
        if not os.path.exists(credentials_file_path):
            self.logger.error(f'Credentials file {credentials_file_path} does not exist')
            raise GoogleSheetsError(f'Credentials file {credentials_file_path} does not exist')

        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            credentials_file_path,
            self.scope,
        )
        self._gc = gspread.authorize(credentials)

    def get_all_records(self, table_url: str, sheet=0):
        worksheet = self.get_worksheet(table_url, sheet)
        return worksheet.get_all_records()

    def get_all_values(self, table_url: str, sheet=0):
        table = self._gc.open_by_url(table_url)
        worksheet = table.get_worksheet(sheet)
        return worksheet.get_all_values()

    def get_worksheet(self, table_url: str, sheet=0):
        table = self._gc.open_by_url(table_url)
        if isinstance(sheet, int):
            worksheet = table.get_worksheet(sheet)
        elif isinstance(sheet, str):
            worksheet = table.worksheet(sheet)
        else:
            raise TypeError('sheet type can be str or int')
        return worksheet

    def create_and_return_worksheet(self, table_url: str, sheet_title: str, rows: str, cols: str):
        table = self._gc.open_by_url(table_url)
        try:
            worksheet = table.worksheet(sheet_title)
            return worksheet
        except Exception as e:
            print(e)
        worksheet = table.add_worksheet(title=sheet_title, rows=rows, cols=cols)
        return worksheet

    def get_header(self, table_url: str, worksheet=0):
        worksheet = self.get_worksheet(table_url, worksheet)
        columns = worksheet.col_count
        begin = f'{self.get_column_name(0)}1'
        end = f'{self.get_column_name(columns - 1)}1'
        cells_list = worksheet.range(f'{begin}:{end}')
        header = [cell.value for cell in cells_list]
        return header

    def create_header(self, worksheet, header: list[str]):
        header_len = len(header)
        begin = f'{self.get_column_name(0)}1'
        end = f'{self.get_column_name(header_len - 1)}1'
        cells_list = worksheet.range(f'{begin}:{end}')
        for i, cell in enumerate(cells_list):
            cell.value = header[i]
        worksheet.update_cells(cells_list)

    def get_all_cells(
        self,
        table_url: str,
        sheet=0,
        column_to_begin=0,
        column_to_end=None,
        row_to_begin=0,
        row_to_end=None,
    ):
        worksheet = self.get_worksheet(table_url, sheet)
        if column_to_end is None:
            column_to_end = worksheet.col_count - 1
        row_to_begin += 1
        if row_to_end is None:
            row_to_end = worksheet.row_count
        begin = f'{self.get_column_name(column_to_begin)}{row_to_begin}'
        end = f'{self.get_column_name(column_to_end)}{row_to_end}'

        return worksheet.range(f'{begin}:{end}')

    @staticmethod
    def get_column_name(column_number):  # From zero
        len_of_alphabet = len(GoogleSheetsReader.alphabet)
        if column_number < len_of_alphabet:
            return GoogleSheetsReader.alphabet[column_number]

        first_letter = GoogleSheetsReader.alphabet[column_number // len_of_alphabet - 1]
        return first_letter + GoogleSheetsReader.alphabet[column_number % len_of_alphabet]

    # def check_header(self, table_url: str, header, sheet: int = 0,
    #                  row_num: int = 0, column_shift: int = 0) -> bool:
    #     all_values = self.get_all_values(table_url, sheet=sheet)
    #     try:
    #         header_from_table = all_values[row_num]
    #     except Exception as e:
    #         logging.error(e)
    #         return False
    #     if len(header_from_table) < len(header) + column_shift:
    #         return False
    #     return all(header_from_table[i - column_shift] == header[i] for i in range(len(header)))

    # @staticmethod
    # def get_all_students(table_url: str, sheet: int = 0, row_start: int = 1):
    #     all_values = GoogleSheetsReader.get_all_values(table_url, sheet=sheet)
    #     header = all_values[0]
    #     result = []
    #     for i in range(row_start, len(all_values)):
    #         s = Student()
    #         s.set(dict(zip(header, all_values[i])))
    #         result += [s]
    #     return result
    #
    # @staticmethod
    # def update_info(table_url: str, column_to_find_name: str,
    #                 column_to_find_value: str, column_to_change_name: str,
    #                 new_value: str, row_start: int = 1, sheet: int = 0):
    #     all_values = GoogleSheetsReader.get_all_values(table_url, sheet=sheet)
    #     header = all_values[0]
    #
    #     if column_to_find_name not in header:
    #         raise LookupError(f"No such column {column_to_find_name} in header {header}")
    #     column_to_find_idx = header.index(column_to_find_name)
    #     if column_to_change_name not in header:
    #         raise LookupError(f"No such column {column_to_change_name} in header {header}")
    #     column_to_change_idx = header.index(column_to_change_name)
    #
    #     worksheet = GoogleSheetsReader.get_worksheet(table_url)
    #     for i, row in enumerate(all_values[row_start:]):
    #         if column_to_find_value == row[column_to_find_idx]:
    #             worksheet.update(f'{GoogleSheetsReader.alphabet[column_to_change_idx]}{i + row_start + 1}', new_value)
    #             return
    #     raise LookupError(f"There is no value {column_to_find_value} in column {column_to_find_name}")
