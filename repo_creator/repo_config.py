import os
import json
import logging
from collections import namedtuple

from configs_helper.config_helper import ConfigHelper


class ConfigException(Exception):
    pass


Submodule = namedtuple("Submodule", [
    'url',
    'name',
    'path',
])


class RepoConfig:
    def __init__(
        self,
        gitlab_url: str,
        gitlab_token: str,
        namespace_to_create_repos: str,
        gitlab_runner_id: str,
        enable_shared_runners: bool,
        ci_file_path: str,
        template_repo_name: str,
        template_repo_namespace: str,
        submodules: list[dict[str, str]],
        files_to_ignore: set[str],
        ci_variables: dict[str, dict[str, [bool, str]]],
        logger=None,
    ):
        self.gitlab_url = gitlab_url
        self.gitlab_token = gitlab_token
        self.namespace_to_create_repos = namespace_to_create_repos
        self.gitlab_runner_id = gitlab_runner_id
        self.enable_shared_runners = enable_shared_runners
        self.ci_file_path = ci_file_path
        self.template_repo_name = template_repo_name
        self.template_repo_namespace = template_repo_namespace
        self.files_to_ignore = files_to_ignore
        self.ci_variables = ci_variables
        self.logger = logger if logger else logging.getLogger(__name__)

        self.submodules = []
        for submodule in submodules:
            self.submodules.append(
                Submodule(**submodule)
            )

    @staticmethod
    def build_from_json(path_to_json: str, logger=None):
        if not os.path.exists(path_to_json):
            raise ConfigException(f'No file {path_to_json}')

        with open(path_to_json, 'r') as json_file:
            config = json.load(json_file)

        return RepoConfig(
            config['gitlab_url'],
            config['gitlab_token'],
            config['namespace_to_create_repos'],
            config['gitlab_runner_id'],
            config['enable_shared_runners'],
            config['ci_file_path'],
            config['template_repo_name'],
            config['template_repo_namespace'],
            config['submodules'],
            set(config['files_to_ignore']),
            config.get('ci_variables', None),
            logger,
        )

    @staticmethod
    def build_from_env(config_name: str, logger=None):
        config_helper = ConfigHelper.build()
        return RepoConfig.build_from_json(
            config_helper.get_repo_config_path(config_name),
            logger
        )
