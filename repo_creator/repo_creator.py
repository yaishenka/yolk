from __future__ import annotations

import logging
from enum import Enum

from gitlab.const import AccessLevel

from repo_creator import repo_config
from gitlab_integration.gitlab_api import GitlabApi, GitlabFileStatus


class RepoCreationError(Exception):
    pass


class CreationStatus(Enum):
    CREATED = 1
    UPDATED = 2
    ALREADY_EXISTS = 3
    ERROR = 4


class RepoCreator:
    def __init__(self, repo_config_name: str, logger=None):
        self.logger = logger if logger else logging.getLogger(__name__)
        self.repo_config = repo_config.RepoConfig.build_from_env(repo_config_name, self.logger)
        self._gitlab_api = GitlabApi(self.repo_config.gitlab_token, self.repo_config.gitlab_url)

        self.logger.info('Getting template repo branches')
        self.template_repo_branches = self._gitlab_api.get_project_branches(
            self.repo_config.template_repo_namespace,
            self.repo_config.template_repo_name,
        )
        if self.template_repo_branches is None:
            self.logger.error('Failed to get template repo branches')
            raise RepoCreationError('Failed to get template repo branches')

        self.branch_files = {}
        self.logger.info('Getting template repo branches files')
        for branch in self.template_repo_branches:
            branch_files = self._gitlab_api.get_branch_files(
                self.repo_config.template_repo_namespace,
                self.repo_config.template_repo_name,
                branch.name,
                self.repo_config.files_to_ignore,
            )
            if branch_files is None:
                self.logger.error(f'Failed to get files from branch {branch.name}')
                raise RepoCreationError(f'Failed to get files from branch {branch.name}')
            self.branch_files[branch.name] = branch_files

    def create_repo(
        self,
        repo_name: str,
        developers: list[str] = None,
        maintainers: list[str] = None,
    ) -> (CreationStatus, [str, None]):
        if developers is None:
            developers = []
        if maintainers is None:
            maintainers = []

        namespace = self.repo_config.namespace_to_create_repos
        self.logger.info(f'Begin creation repo {repo_name} in namespace {namespace}')
        if self._gitlab_api.is_project_exists(namespace, repo_name):
            self.logger.info('Project already exists')
            return CreationStatus.ALREADY_EXISTS, self._gitlab_api.get_project_url(namespace, repo_name)

        default_branch_name = self._get_default_branch_name()
        repo_url = self._gitlab_api.create_project(
            namespace,
            repo_name,
            default_branch_name,
        )
        if repo_url is None:
            self.logger.error('Failed to create repo')
            return CreationStatus.ERROR, None

        self.logger.info('Clone default branch')
        status = self._clone_branch(namespace, repo_name, default_branch_name, default_branch_name)
        if status != CreationStatus.CREATED:
            return status, repo_url

        self.logger.info('Adding submodules')
        status = self._add_submodules(namespace, repo_name, default_branch_name)
        if status != CreationStatus.CREATED:
            return status, repo_url

        self.logger.info('Cloning branches')
        status = self._clone_branches(namespace, repo_name, default_branch_name)
        if status != CreationStatus.CREATED:
            return status, repo_url

        self.logger.info('Setup CI')
        status = self._setup_ci(namespace, repo_name)
        if status != CreationStatus.CREATED:
            return status, repo_url

        self.logger.info('Giving access')
        status = self._set_access_level(namespace, repo_name, maintainers, AccessLevel.MAINTAINER)
        if status != CreationStatus.CREATED:
            return status, repo_url

        status = self._set_access_level(namespace, repo_name, developers, AccessLevel.DEVELOPER)
        if status != CreationStatus.CREATED:
            return status, repo_url

        return CreationStatus.CREATED, repo_url

    def update_repo(
        self,
        repo_name: str,
        branches_to_update: set[str] = None,
        remove_access_users: list[str] = None,
        maintainers_to_add: list[str] = None,
        developers_to_add: list[str] = None,
        force=False,
    ):
        """Update repo

        branches_to_update -- list of branches to update. None = all (default None)
        force - override files content (default False)
        """
        if remove_access_users is None:
            remove_access_users = []
        if maintainers_to_add is None:
            maintainers_to_add = []
        if developers_to_add is None:
            developers_to_add = []

        namespace = self.repo_config.namespace_to_create_repos
        self.logger.info(f'Begin updating repo {repo_name} in namespace {namespace}')
        if not self._gitlab_api.is_project_exists(namespace, repo_name):
            self.logger.info('Project not exists. Creating project')
            return self.create_repo(repo_name, developers_to_add, maintainers_to_add)

        repo_url = self._gitlab_api.get_project_url(namespace, repo_name)
        default_branch_name = self._get_default_branch_name()

        self.logger.info('Updating branches')
        status = self._update_branches(namespace, repo_name, default_branch_name, branches_to_update, force)
        if status == CreationStatus.ERROR:
            return status, repo_url

        self.logger.info('Setup CI')
        status = self._setup_ci(namespace, repo_name)
        if status == CreationStatus.ERROR:
            return status, repo_url

        self.logger.info('Updating access')
        status = self._set_access_level(namespace, repo_name, remove_access_users, AccessLevel.NO_ACCESS)
        if status == CreationStatus.ERROR:
            return status, repo_url

        status = self._set_access_level(namespace, repo_name, maintainers_to_add, AccessLevel.MAINTAINER)
        if status == CreationStatus.ERROR:
            return status, repo_url

        status = self._set_access_level(namespace, repo_name, developers_to_add, AccessLevel.DEVELOPER)
        if status == CreationStatus.ERROR:
            return status, repo_url

        return CreationStatus.UPDATED, repo_url

    def update_access(
        self,
        repo_name: str,
        remove_access_users: list[str] = None,
        maintainers_to_add: list[str] = None,
        developers_to_add: list[str] = None,
    ):
        if remove_access_users is None:
            remove_access_users = []
        if maintainers_to_add is None:
            maintainers_to_add = []
        if developers_to_add is None:
            developers_to_add = []

        namespace = self.repo_config.namespace_to_create_repos
        self.logger.info(f'Begin updating repo {repo_name} in namespace {namespace}')
        if not self._gitlab_api.is_project_exists(namespace, repo_name):
            self.logger.info('Project not exists. Creating project')
            return self.create_repo(repo_name, developers_to_add, maintainers_to_add)

        self.logger.info('Updating access')
        status = self._set_access_level(namespace, repo_name, remove_access_users, AccessLevel.NO_ACCESS)
        if status == CreationStatus.ERROR:
            return status

        status = self._set_access_level(namespace, repo_name, maintainers_to_add, AccessLevel.MAINTAINER)
        if status == CreationStatus.ERROR:
            return status

        status = self._set_access_level(namespace, repo_name, developers_to_add, AccessLevel.DEVELOPER)
        if status == CreationStatus.ERROR:
            return status

        return CreationStatus.UPDATED

    def _set_access_level(
        self,
        namespace: str,
        repo_name: str,
        users: list[str],
        access_level: AccessLevel,
    ):
        for user in users:
            self.logger.info(f'Set level {access_level} to {user}')
            if access_level == AccessLevel.NO_ACCESS:
                member = self._gitlab_api.remove_access(
                    namespace,
                    repo_name,
                    user
                )
            else:
                member = self._gitlab_api.give_access(
                    namespace,
                    repo_name,
                    user,
                    access_level,
                )
            if member is None:
                self.logger.error('Failed to set access level')
                return CreationStatus.ERROR

        return CreationStatus.CREATED

    def _add_submodules(self, namespace: str, repo_name: str, default_branch: str):
        for submodule in self.repo_config.submodules:
            self.logger.info(f'Adding submodule {submodule.url}')
            submodule_added = self._gitlab_api.add_submodule_to_project(
                namespace,
                repo_name,
                submodule.url,
                submodule.name,
                submodule.path,
                default_branch,
            )
            if not submodule_added:
                self.logger.error(f'Failed to add submodule {submodule.url}')
                return CreationStatus.ERROR

        return CreationStatus.CREATED

    def _setup_ci(self, namespace: str, repo_name: str):
        if not self.repo_config.enable_shared_runners:
            self.logger.info('Disabling shared runners')
            status = self._gitlab_api.disable_shared_runners(namespace, repo_name)
            if status is None:
                self.logger.error('Failed to disable shared runners')
                return CreationStatus.ERROR

        if self.repo_config.gitlab_runner_id:
            self.logger.info(f'Registering runner {self.repo_config.gitlab_runner_id}')
            runner = self._gitlab_api.add_runner_to_project(
                namespace,
                repo_name,
                self.repo_config.gitlab_runner_id,
            )
            if runner is None:
                self.logger.error('Failed to register runner')
                return CreationStatus.ERROR

        if self.repo_config.ci_file_path:
            self.logger.info(f'Setting path to ci file to {self.repo_config.ci_file_path}')
            ci_file_path = self._gitlab_api.set_path_to_ci_file(
                namespace,
                repo_name,
                self.repo_config.ci_file_path
            )
            if ci_file_path is None:
                self.logger.error('Failed to set ci file path')
                return CreationStatus.ERROR

        if self.repo_config.ci_variables is not None:
            self.logger.info('Setup CI variables')
            for key, variable in self.repo_config.ci_variables.items():
                self.logger.debug(f'Set var {key}')
                key_set = self._gitlab_api.set_ci_var(
                    namespace,
                    repo_name,
                    key,
                    variable["value"],
                    force_update=True,
                    protected=variable.get("protected", False),
                    masked=variable.get("masked", True),
                )
                if key_set is None:
                    self.logger.error(f'Failed to set variable {key}')
                    return CreationStatus.ERROR

        return CreationStatus.CREATED

    def _update_branches(
        self,
        namespace: str,
        repo_name: str,
        default_branch_name: str,
        branches_to_update: set[str] = None,
        force=False,
    ):
        for branch in self.template_repo_branches:
            if branches_to_update and branch.name not in branches_to_update:
                self.logger.info(f'Skip branch {branch.name}')
                continue

            if branch.default:
                # TODO(yaishenka) implement rebase and update default branch
                self.logger.info('Not updating default branch. Rebase not implemented')
                continue

            status = self._clone_branch(namespace, repo_name, branch.name, default_branch_name, force)
            if status == CreationStatus.ERROR:
                return status

        return CreationStatus.UPDATED

    def _clone_branches(self, namespace: str, repo_name: str, default_branch_name: str):
        for branch in self.template_repo_branches:
            if branch.default:
                continue
            status = self._clone_branch(namespace, repo_name, branch.name, default_branch_name)
            if status != CreationStatus.CREATED:
                return status

        return CreationStatus.CREATED

    def _clone_branch(
        self,
        namespace: str,
        repo_name: str,
        branch_name: str,
        default_branch: str,
        update=False,
    ):
        self.logger.info(f'Clone branch {branch_name}')
        if branch_name != default_branch:
            self.logger.info(f'Creating branch {branch_name}')
            created_branch = self._gitlab_api.create_branch(
                namespace,
                repo_name,
                branch_name,
                default_branch,
            )
            if created_branch is None:
                self.logger.error(f'Failed to create branch {branch_name}')
                return None
        branch_files = self.branch_files[branch_name]
        added_files = self._gitlab_api.add_files_to_branch(
            namespace,
            repo_name,
            branch_name,
            branch_files,
            update,
            default_branch,
        )

        if added_files is None:
            return CreationStatus.ERROR

        for _, status in added_files:
            if status == GitlabFileStatus.FAILED:
                self.logger.error(f'Failed to clone branch {branch_name}')
                return CreationStatus.ERROR

        return CreationStatus.CREATED

    def _get_default_branch_name(self):
        for branch in self.template_repo_branches:
            if branch.default:
                return branch.name

        raise RepoCreationError('No default branch in template repo')
