import os
import json
import logging
from repo_creator import repo_config
from moss_integration import plagiarism_config
from students import student_sheet_api
from students.student import Student
from configs_helper.config_helper import ConfigHelper
from google_sheets_integration import google_sheets_reader


class CourseConfigException(Exception):
    pass


class CourseConfig:
    def __init__(
        self,
        repo_config_name: str,
        student_template_repo_name: str,
        table_url: str,
        google_auth_file_name: str,
        tasks_sheet: int,
        course_repo_namespace: str,
        course_repo_name: str,
        course_main_branch: str,
        deadlines_file_path: str,
        plagiarism_config_name: str,
        logger=None,
    ):
        self.repo_config_name = repo_config_name
        self.student_template_repo_name = student_template_repo_name
        self.table_url = table_url
        self.google_auth_file_name = google_auth_file_name
        self.tasks_sheet = tasks_sheet
        self.course_repo_namespace = course_repo_namespace
        self.course_repo_name = course_repo_name
        self.course_main_branch = course_main_branch
        self.deadlines_file_path = deadlines_file_path
        self.plagiarism_config_name = plagiarism_config_name
        self.logger = logger if logger else logging.getLogger(__name__)

    def get_repo_config(self):
        return repo_config.RepoConfig.build_from_env(
            self.repo_config_name,
            self.logger
        )

    def get_plagiarism_config(self):
        if self.plagiarism_config_name:
            return plagiarism_config.PlagiarismConfig.build_from_env(
                self.plagiarism_config_name,
                self.logger
            )

        return None

    def get_student_sheet_api(self):
        config_helper = ConfigHelper.build()
        return student_sheet_api.StudentSheetApi(
            self.table_url,
            config_helper.get_google_auth_file(self.google_auth_file_name),
            self.tasks_sheet,
            self.logger,
        )

    def get_google_sheets_api(self):
        config_helper = ConfigHelper.build()
        return google_sheets_reader.GoogleSheetsReader(
            config_helper.get_google_auth_file(self.google_auth_file_name),
            self.logger,
        )

    def get_student_repo_name(self, student: Student):
        return self.student_template_repo_name.format(**student.__dict__)

    @staticmethod
    def build_from_json(path_to_json: str, logger=None):
        if not os.path.exists(path_to_json):
            raise CourseConfigException(f'No file {path_to_json}')

        with open(path_to_json, 'r') as json_file:
            config = json.load(json_file)

        return CourseConfig(
            config['repo_config_name'],
            config['student_template_repo_name'],
            config['table_url'],
            config['google_auth_file_name'],
            config['tasks_sheet'],
            config['course_repo_namespace'],
            config['course_repo_name'],
            config['course_main_branch'],
            config['deadlines_file_path'],
            config.get('plagiarism_config_name', ''),
            logger,
        )

    @staticmethod
    def build_from_env(config_name: str, logger=None):
        config_helper = ConfigHelper.build()
        return CourseConfig.build_from_json(
            config_helper.get_course_config_path(config_name),
            logger
        )

