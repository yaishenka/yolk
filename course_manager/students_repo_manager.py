import logging
from course_manager.course_config import CourseConfig
from repo_creator import repo_creator


class StudentsRepoManager:
    def __init__(
        self,
        course_config_name: str,
        logger=None,
        set_creation_status=False,
        set_repo_url=False,
    ):
        self.logger = logger if logger else logging.getLogger(__name__)
        self.course_config = CourseConfig.build_from_env(course_config_name, self.logger)
        self.set_creation_status = set_creation_status
        self.set_repo_url = set_repo_url

        self._repo_creator = repo_creator.RepoCreator(
            self.course_config.repo_config_name,
            self.logger
        )
        self._student_sheet_api = self.course_config.get_student_sheet_api()

    def create_all_repos(self):
        all_students = self._student_sheet_api.get_all_students()
        student_to_creation_status = {}
        for student in all_students:
            student_to_creation_status[student] = {}
            repo_name = self.course_config.get_student_repo_name(student)
            self.logger.info(f"Create repo {repo_name} for student {student}")

            maintainers = []
            if student.reviewers is not None:
                maintainers.extend(student.reviewers)
            creation_status, repo_url = self._repo_creator.create_repo(
                repo_name,
                developers=[student.gitlab_login, ],
                maintainers=maintainers,
                # TODO devs and maintainers from google table
            )
            if self.set_creation_status:
                student_to_creation_status[student]['creation_status'] = creation_status.name

            if self.set_repo_url:
                student_to_creation_status[student]['repo_url'] = repo_url

        if self.set_creation_status or self.set_repo_url:
            self._student_sheet_api.update_students_batch(
                student_to_creation_status
            )

    def update_all_repos(self, force=False):
        all_students = self._student_sheet_api.get_all_students()
        student_to_creation_status = {}
        for student in all_students:
            student_to_creation_status[student] = {}
            repo_name = self.course_config.get_student_repo_name(student)
            self.logger.info(f"Update repo {repo_name} for student {student}")
            maintainers = []
            if student.reviewers is not None:
                maintainers.extend(student.reviewers)
            creation_status, repo_url = self._repo_creator.update_repo(
                repo_name,
                force=force,
                developers_to_add=[student.gitlab_login, ],
                maintainers_to_add=maintainers,
            )
            if self.set_creation_status:
                student_to_creation_status[student]['creation_status'] = creation_status.name

            if self.set_repo_url:
                student_to_creation_status[student]['repo_url'] = repo_url

        if self.set_creation_status or self.set_repo_url:
            self._student_sheet_api.update_students_batch(
                student_to_creation_status
            )

    def update_access_for_all_repos(self):
        all_students = self._student_sheet_api.get_all_students()
        student_to_creation_status = {}
        for student in all_students:
            student_to_creation_status[student] = {}
            repo_name = self.course_config.get_student_repo_name(student)
            self.logger.info(f"Update access for repo {repo_name} for student {student}")
            maintainers = []
            if student.reviewers is not None:
                maintainers.extend(student.reviewers)
            creation_status = self._repo_creator.update_access(
                repo_name,
                developers_to_add=[student.gitlab_login, ],
                maintainers_to_add=maintainers,
            )
            if self.set_creation_status:
                student_to_creation_status[student]['creation_status'] = creation_status.name

        if self.set_creation_status:
            self._student_sheet_api.update_students_batch(
                student_to_creation_status
            )
