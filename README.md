[![pylint](https://gitlab.com/yaishenka/yolk/-/jobs/artifacts/main/raw/pylint/pylint.svg?job=pylint)](https://gitlab.com/yaishenka/yolk/-/jobs/artifacts/main/raw/pylint/pylint.log?job=pylint)

# yolk 

Этот небольшой фреймворк позволяет заводить репозитории в GitLab для работы с домашними заданиями

# Гайд по скриптам

## Update/Create

Для create 

     python3 -m scripts.create_repos --config-folder /Users/d.gagarinov/repos/yolk_config/configs --verbose --course-config hsse_cpp_course --set-creation-status --set-repo_url

Где: 

* --config-folder Путь до папки в которой лежат конфиги (наши лежат в преватной репе yolk_config)
* --verbose Ставит уровень принта в debug
* --course-config название файла с конфигом курса (можно с .json)
* --set-creation-status выставляет колонку creation_status в таблице. Для этого в таблице должна быть такая колонка!!!
* --set-repo_url выставляет колонку repo_url в таблице. Для этого в таблице должна быть такая колонка!!!

Для update все тоже самое, только можно добавить --force (это заставит скрипт перезатереть файлы в репах студентов)

## update_students_progress

    python3 -m scripts.update_repos --config-folder /Users/d.gagarinov/repos/yolk_config/configs --verbose --course-config hsse_cpp_course --set-creation-status --set-repo_url
 

Проверяет все МРы в репозиториях студентов и выставляет баллы в табличку 
Дедлайны берет из репозитория курсов (ссылка на которой должна быть в конфиге курса)

## create_plagiarism_report
    
    python3 -m scripts.create_plagiarism_report --config-folder /Users/d.gagarinov/repos/yolk_config/configs --verbose --course-config hsse_cpp_course 

Скачивает все задачи из гитлаба
Отправляет файлы в moss
Генерирует отчет в гугл-табличке
