import logging
import os
import shutil

import mosspy
from bs4 import BeautifulSoup
from tqdm import tqdm

from course_manager import course_config
from gitlab_integration.gitlab_api import GitlabApi


class PlagiarismCheckerException(Exception):
    pass


class PlagiarismChecker:
    def __init__(self, course_config_name: str, logger=None):
        self.logger = logger if logger else logging.getLogger(__name__)
        self.course_config = course_config.CourseConfig.build_from_env(course_config_name, self.logger)
        self.repo_config = self.course_config.get_repo_config()
        self.plagiarism_config = self.course_config.get_plagiarism_config()
        if not self.plagiarism_config:
            msg = 'You should provide plagiarism config for course'
            self.logger.error(msg)
            raise PlagiarismCheckerException(msg)

        self._gitlab_api = GitlabApi(self.repo_config.gitlab_token, self.repo_config.gitlab_url)
        self._student_sheet_api = self.course_config.get_student_sheet_api()
        self._google_sheets_reader = self.course_config.get_google_sheets_api()

    def download_data(self):
        if not os.path.exists(self.plagiarism_config.path_to_store_data):
            os.mkdir(self.plagiarism_config.path_to_store_data)
        all_students = self._student_sheet_api.get_all_students()
        for branch_name in tqdm(self.plagiarism_config.branches_to_check, desc='Downloading data'):
            path_to_store_data = os.path.join(self.plagiarism_config.path_to_store_data, branch_name)
            if not os.path.exists(path_to_store_data):
                os.mkdir(path_to_store_data)

            for student in tqdm(all_students):
                student_repo = self.course_config.get_student_repo_name(student)
                branch_files = self._gitlab_api.get_branch_files(self.repo_config.namespace_to_create_repos,
                                                                 student_repo, branch_name,
                                                                 files_to_ignore=['README.md', ], in_bytes=True)
                if not branch_files:
                    self.logger.error(f'No files in branch {branch_name}. Looking for files in main branch')
                    default_branch = self._gitlab_api.get_default_branch_name(
                        self.repo_config.namespace_to_create_repos, student_repo)
                    branch_files = self._gitlab_api.get_branch_files(self.repo_config.namespace_to_create_repos,
                                                                     student_repo, default_branch,
                                                                     files_to_ignore=['README.md', ], in_bytes=True)
                all_files_content = b''
                for file in branch_files:
                    if not file.path.startswith(branch_name):
                        continue
                    skip_file = True
                    for extension in self.plagiarism_config.extensions_to_check:
                        if file.path.endswith(extension):
                            skip_file = False
                            break
                    if skip_file:
                        continue
                    all_files_content += file.content
                    all_files_content += b'\n'
                student_file_name = f'{branch_name}#{student.gitlab_login}#{student_repo}'
                if not all_files_content:
                    continue
                with open(os.path.join(path_to_store_data, student_file_name), 'wb') as f:
                    f.write(all_files_content)

    def create_report(self):
        if not os.path.exists(self.plagiarism_config.path_to_store_reports):
            os.mkdir(self.plagiarism_config.path_to_store_reports)

        for branch_name in tqdm(self.plagiarism_config.branches_to_check, desc='Creating reports'):
            for retry in range(self.plagiarism_config.retry_count):
                try:
                    path_to_store_reports = os.path.join(self.plagiarism_config.path_to_store_reports, branch_name)
                    if os.path.exists(path_to_store_reports):
                        shutil.rmtree(path_to_store_reports, ignore_errors=True)
                    os.mkdir(path_to_store_reports)
                    moss = mosspy.Moss(self.plagiarism_config.moss_user_id, self.plagiarism_config.moss_language)
                    moss.addFilesByWildcard(os.path.join(self.plagiarism_config.path_to_store_data, branch_name, '*'))
                    url = moss.send()
                    self.logger.info(f'Url for problem {branch_name} is {url}')
                    moss.saveWebPage(url, os.path.join(path_to_store_reports, 'main.html'))
                    mosspy.download_report(url, os.path.join(path_to_store_reports, 'reports'))
                    break
                except Exception as e:
                    self.logger.error(f"Can't generate report. Retry {retry + 1}. Exception {e}")

    def create_google_sheets_report(self):
        result = self._parse_reports()
        gitlab_nick_to_student_name = {}
        for student in self._student_sheet_api.get_all_students():
            gitlab_nick_to_student_name[student.gitlab_login] = student.name
        for task_name in tqdm(self.plagiarism_config.branches_to_check, desc='Creating google sheets report'):
            task_results = result[task_name]
            sheet = self._google_sheets_reader.create_and_return_worksheet(self.plagiarism_config.plagiarism_table,
                                                                           task_name,
                                                                           str(len(task_results) + 1),
                                                                           str(6))
            self._google_sheets_reader.create_header(
                sheet,
                [
                    'left_participant',
                    'left_percent',
                    'right_participant',
                    'right_percent',
                    'lines_matched',
                    'href',
                ]
            )
            cells_list = sheet.range(f'A2:F{len(task_results) + 1}')
            for i, task_result in enumerate(task_results):
                row = i * 6
                cells_list[row].value = gitlab_nick_to_student_name[
                    self._get_participant_nick_from_file(task_result['file_name1'])]
                cells_list[row + 1].value = task_result['percent1']
                cells_list[row + 2].value = gitlab_nick_to_student_name[
                    self._get_participant_nick_from_file(task_result['file_name2'])]
                cells_list[row + 3].value = task_result['percent2']
                cells_list[row + 4].value = task_result['lines_matched']
                cells_list[row + 5].value = task_result['href']
            sheet.update_cells(cells_list)

    @staticmethod
    def _get_participant_nick_from_file(file_name):
        gitlab_nick = file_name.split('#')[1]
        return gitlab_nick

    def _parse_reports(self):
        result = {}
        for task_name in self.plagiarism_config.branches_to_check:
            task_result = []
            main_report_file = os.path.join(self.plagiarism_config.path_to_store_reports, task_name, 'main.html')
            with open(main_report_file) as fp:
                soup = BeautifulSoup(fp, 'html.parser')
            table = soup.find('table')
            table = str(table)
            table_lines = [line for line in table.split('\n') if line][2::]
            table_lines.pop()
            for i in range(0, len(table_lines), 3):
                href, file_name1, percent1 = self._parse_line_with_ref(table_lines[i])
                _, file_name2, percent2 = self._parse_line_with_ref(table_lines[i + 1])
                lines_matched = int(table_lines[i + 2].split('>')[1])
                task_result.append({
                    'href': href,
                    'file_name1': file_name1,
                    'percent1': percent1,
                    'file_name2': file_name2,
                    'percent2': percent2,
                    'lines_matched': lines_matched,
                })
            task_result.sort(key=lambda row: row['lines_matched'], reverse=True)
            result[task_name] = task_result

        return result

    @staticmethod
    def _parse_line_with_ref(ref_line):
        ref_line_soup = BeautifulSoup(ref_line, 'html.parser')
        a_tag = ref_line_soup.find('a')
        href = a_tag['href']
        file_name = a_tag.text.split(' ')[0]
        percent = float(a_tag.text.split(' ')[1].replace('(', '').replace(')', '').replace('%', ''))
        return href, file_name, percent
