import os
import json
import logging
from configs_helper.config_helper import ConfigHelper


class PlagiarismConfigException(Exception):
    pass


class PlagiarismConfig:
    def __init__(
        self,
        plagiarism_table: str,
        moss_user_id: int,
        moss_language: str,
        path_to_store_data: str,
        path_to_store_reports: str,
        retry_count: str,
        repos_to_exclude: list[str],
        branches_to_check: list[str],
        extensions_to_check: list[str],
        logger=None,
    ):
        self.plagiarism_table = plagiarism_table
        self.moss_user_id = moss_user_id
        self.moss_language = moss_language
        self.path_to_store_data = path_to_store_data
        self.path_to_store_reports = path_to_store_reports
        self.retry_count = retry_count
        self.repos_to_exclude = repos_to_exclude
        self.branches_to_check = branches_to_check
        self.extensions_to_check = extensions_to_check
        self.logger = logger if logger else logging.getLogger(__name__)

    @staticmethod
    def build_from_json(path_to_json: str, logger=None):
        if not os.path.exists(path_to_json):
            raise PlagiarismConfigException(f'No file {path_to_json}')

        with open(path_to_json, 'r') as json_file:
            config = json.load(json_file)

        return PlagiarismConfig(
            config['plagiarism_table'],
            config['moss_user_id'],
            config['moss_language'],
            config['path_to_store_data'],
            config['path_to_store_reports'],
            config['retry_count'],
            config['repos_to_exclude'],
            config['branches_to_check'],
            config['extensions_to_check'],
            logger,
        )

    @staticmethod
    def build_from_env(config_name: str, logger=None):
        config_helper = ConfigHelper.build()
        return PlagiarismConfig.build_from_json(
            config_helper.get_plagiarism_config_path(config_name),
            logger
        )

